# Java Hello World

This project provides a minimal Java 17 development environment in a  Visual Studio Code Dev Container.

## Features

- Java 17 installed in a Docker container
- [Extension Pack for Java VS Code extension](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack) (vscjava.vscode-java-pack)

## Requires

* Docker
* VS Code
* VS Code extension "Remote - Containers"

## Using

1. Create a new file - name it with the `.java` extension
2. Add code to the file
3. Click on the `Run Java` button in the upper right of the window

## Notes

1. This is an example that is meant to show you the simplest possible Java development environment.
2. You will likely want to install libraries.

---
Copyright &copy; 2022 The HFOSSedu Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.